# Docker-Compose services

## Overview

This repository contains the manage-compose-services.sh script that allows for 
managing docker-compose based services:

* Manage docker-compose based services as native system services. Only systemd is 
  currently supported, but support for additional init systems can be easily added.
  This functionality allows for starting, stopping, enabling and disabling one or
  more docker-compose based services as systemd units.
  
* Create and remove cron jobs on the host system based on instructions in the 
  docker-compose files. This allows for scheduling service-related tasks like
  updating the Docker images for a service, restarting service containers, 
  and running other maintenance activities.

* Run arbitrary host commands based on instructions in the docker-compose files. 
  For example, this allows for installing host system prerequisites (like kernel 
  modules), or running predefined on-demand maintenance tasks.
  
* Run arbitrary commands on a given set of services. For example, you could use 
  this to run a git commit and push for all defined services.
  
* Clone all docker-compose service repositories from a GitLab group.

Each docker-compose based service is represented by a directory containing
a docker-compose.yml file. By default, these docker-compose based service
directories are expected to be located in the `services/impl` directory 
underneath the directory containing the manage-compose-services.sh script.
Persistent data is by default stored in the `services/data` directory; the 
script will automatically set up a symbolic link from `services/impl/<service>/data`
to `services/data/<service>`. 

As an example, a portainer docker-compose.yml file is included in this 
repository. Most of the examples in this document are based on this 
portainer service.

### Background

For my own home server, I was looking for a simple solution that allows for the 
following:

* Use docker-compose to define the various services provided by my home 
  server, allowing me to try out new services without polluting the host 
  system, and having all service-related configuration in a single place/directory. 
* Automatically starting and stopping docker-compose based services upon
  system start/shutdown.
* Automated scheduled updates of Docker images used by these docker-compose
  based services, to benefit from (security) patches and other updates.
* Easily rebuild my complete home server environment:

    * Install script prerequisites:
          * Docker
          * Git
          * curl    
    * Clone the contents of the manage-compose-services
      repository
    * Run the script to automatically download all 
      docker-compose services from one or more private GitLab 
      groups
    * Run the script to automatically configure the host system 
      (like installing kernel modules and enabling docker-compose
      services as native system services) based on instructions 
      included with the docker-compose services
* For example, the following commands could rebuild my complete home server environment:
     
     ```
     sudo su
     apt-get update
     apt-get install -y docker git curl
     mkdir -p /opt/docker && cd /opt/docker
     git clone https://gitlab.com/rsenden-public-docker/manage-compose-services.git
     cd manage-compose-services
     ./manage-compose-services.sh configure
     ./manage-compose-services.sh gitlab cloneGroup https://gitlab.com <personal access token> <group containing docker-compose based service repositories>
     ./manage-compose-services.sh run install services/impl/*
     ```

There are many Docker-related management tools available, but these come with
an additional learning curve, may be overkill for managing Docker services on
a simple home server, and most of these solutions may not provide all required 
functionality out of the box. As such I decided to develop this script.

## docker-compose based services

You can use docker-compose files as usual to define the services to be managed
with `manage-compose-services.sh`, with some additional settings and
considerdations:

* The docker-compose files can be amended with comments that provide instructions 
  for the `manage-compose-services.sh`.
* Persistent data should be stored under the `data` subdirectory within the
  directory containing the docker-compose.yml file.
* The main service definition should be in a file named docker-compose.yml. The
  service directory may contain additional docker-compose*.yml files. Any services
  defined in such files will not be managed with the host init system, but they
  may contain instructional comments. Such docker-compose*.yml files usually contain
  maintenance services that are either scheduled or can be run ad-hoc.

See the following sections for more details. 

### Handling persistent data

Docker-compose files should map persistent data directories to (sub-directories of) the data 
directory, for example:

```
   volumes:
      - ./data/dir1:/container/dir1:rw
      - ./data/dir2:/container/dir2:ro
```

The `manage-compose-services.sh` script will create a symbolic link from this data 
directory to a service-specific data directory under the configured base service data directory. 

For example, suppose the following:

* The `manage-compose-services.sh` has been installed to `/opt/docker/manage-compose-services`
* You have run `./manage-compose-services.sh configure` with default settings
* You have run `./manage-compose-services.sh run install portainer`

This will result in the following situation:

* A directory `/opt/docker/manage-compose-services/services/data/portainer` has been created
* A symbolic link from `/opt/docker/manage-compose-services/services/impl/portainer/data` to 
  `/opt/docker/manage-compose-services/services/data/portainer` has been created
* The docker-compose file maps container directories to `/opt/docker/manage-compose-services/services/impl/portainer/data`
* Due to the symbolic link, the actual data is stored under `/opt/docker/manage-compose-services/services/data/portainer`

Storing persistent data in this way has several advantages:

* All persistent data is stored in a single place, making it easier to back up this persistent data.
* The actual service definition directory only contains static files (that are usually stored in a
  source code repository and thus doesn't need to be backed up).
* You can easily remove a full service definition directory while maintaining the service's data
 
### docker-compose instruction comments

The following sections describe the `manage-compose-services.sh` instructions that can be embedded in docker-compose
files as comments. Note that any of these comments must match the exact format (including number of spaces) as described
in these sections.

#### Host commands
      
##### `# HOST_CMD <name> <command>`

Define a host command with the given name. The same `<name>` can be used multiple times. These
host commands can be invoked using the `./manage-compose-services.sh run <name> <service names>`
command. This will run all `HOST_CMD` commands with the given `<name>` for all services
specified by `<service names>`. 

Usually, each main docker-compose.yml file will provide at least the following two HOST_CMD instructions:

* `# HOST_CMD install <install commands>`
* `# HOST_CMD uninstall <uninstall commands>`

These commands would perform any host system preparations, like installing kernel modules required for
correct service operation, and call the `_service <action>` (see below) to set up the service data 
directory, install host system cron jobs, and optionally enable the docker-compose service as a native
host system service. 

###### Functions and Variables

The following functions can be called in `HOST_CMD` instructions:

* `_ <action> <action arguments>`: Run an arbitrary manage-compose-services action
  for the current service, for example `_ service reload`.
* `_dc <docker-compose action>`: Run an arbitrary docker-compose action for the current
  service. This will invoke docker-compose with the following parameters:
  
    * `-f <main docker-compose.yml file for current service>`
    * `-f <docker-compose*.yml file that contains the current HOST_CMD instruction>`
    * All parameters given to the `_dc` function
* `_service <action>`: Install or uninstall the current service as a host system
  service. The following actions are available:
  
     * `installAsSystemService`: Sets up the data directory, starts
        and enables the docker-compose service as a native host system 
        service, and installs any cron jobs defined through CRON
        instructions.
     * `installAsSystemServiceNoStart`: Same as above, but
        doesn't actually start the service right away. The service
        will be enabled though, so it will be started automatically
        upon next reboot.
     * `installCronOnly`: Only sets up the data directory and installs
        any cron jobs defined through CRON instructions. This is mainly
        used for docker-compose services that do not run all the time,
        like services that perform daily back-ups.

The following variables can be references in `HOST_CMD` instructions:

* `${compose_dir}`: Service directory that contains the docker-compose files for
  the service currently being processed.
* `${compose_file}`: When used with the `run` action, this variable contains the
  fully qualified name of the main docker-compose.yml file. When used within the 
  `HOST_CMD` instruction, this variable contains the fully qualified name of the
  docker-compose file that contains the `HOST_CMD` instruction.
* `${compose_name}`: Similar to `${compose_file}`, but containing only the
  name of the docker-compose file, without the path.
* `${service_name}`: Name of the service currently being processed.

In addition, the following global variables can be accessed:

* `${SCRIPT_NAME}`: Simple name of the main script, which will be `manage-compose-services.sh`
  unless you renamed the script.
* `${SCRIPT}`: Fully qualified name including directory for the main script.
* `${SCRIPT_DIR}`: Directory where the main script is located.
* `${SERVICES_IMPL_DIR}`: Base directory containing the docker-compose based
  services (i.e. the parent directory of `${compose_dir}` mentioned above). 
* `${SERVICES_DATA_DIR}`: Base directory containing the docker-compose based
  services data.
* `${SERVICES_INIT_SYSTEM}`: The host init system currently in use for managing
  docker-compose services as native host system services.

#### Cron jobs

These instructions allow for defining host system cron jobs. Cron jobs can be installed
on the host system using `./manage-compose-services.sh cron install <service names>`,
and removed using `./manage-compose-services.sh cron remove <service names>`.
Usually there is no need to run these commands manually; instead each service should define
a `HOST_CMD install` instruction that takes care of installing and uninstalling cron
jobs amongst other install and uninstall actions. Cron jobs will be created in 
`/etc/cron.d/<service name>`.

##### `# CRON <minute> <hour> <day of month> <month> <day of week> <user> <HOST_CMD name>`

Define a cron job on the host system that runs the host command as configured through the
HOST\_CMD action with the given HOST\_CMD name with the given user. As an example, the following
two instructions define a cron job that echo's 'Hello' to '/tmp/hello1.txt' every minute:

```
# HOST_CMD hello echo 'Hello' >> /tmp/hello1.txt
# CRON * * * * * root hello
```

## Managing services

The following commands can be used to run the portainer service as a native systemd service:

* `./manage-compose-services.sh configure`

    Only needs to be run once, to copy the generic systemd unit and timer
    files to `/etc/systemd/system/`, and generate the configuration file.
    Note that you may need to manually enable/start system services apart 
    from `docker-compose@.service`. This command takes optional parameters
    for specifying base directories and the host init system to use.

* `./manage-compose-services.sh run install portainer`

    Runs the `HOST_CMD install` instruction from the portainer docker-compose.yml
    file, which in turn sets up the persistent data directory, starts and enables 
    the docker-compose portainer service as a native system service, and generates 
    crontab entries for service maintenance. 

* `./manage-compose-services.sh run uninstall portainer`

    Stops and disables docker-compose portainer service as a native system service,
    and removes any related crontab entries. 
    
You can also manually run systemctl commands on docker-compose services, for example:

* `./manage-compose-services.sh service restart portainer`

     Restarts the native host system service for the portainer docker-compose service.
     
* `./manage-compose-services.sh service start portainer`

     Start portainer as a system service, but does not enable it for running
     at system start, and will not generate the corresponding cron jobs.

Finally, you can also just update or remove the cron jobs for given services with the following commands:

* `./manage-compose-services.sh cron install portainer`
* `./manage-compose-services.sh cron remove portainer`

## Running commands

Commands can be run on services directly using the 'run cmd' action, or you can
run commands as configured through the HOST_CMD instructions in docker-compose
files using the 'run <host_cmd name>' action:

* `./manage-compose-services.sh run cmd 'echo "Hello1 from ${service_name} (${compose_name})"' portainer`

     Run the given echo command for the portainer service,
     outputting the message `Hello1 from portainer (docker-compose.yml)`.
    
* `./manage-compose-services.sh run hello1 portainer`

     Executes all HOST_CMD instructions with the given name 
     (`hello1` in this example) from all docker-compose files 
     for the portainer service.

### Example

As an example, let's have a look at the docker-compose files for the portainer example. These
files contain the following `HOST_CMD` instructions:

* docker-compose.yml:

      * `# HOST_CMD hello1 echo "Hello1 from ${service_name} (${compose_name})"`
      * `# HOST_CMD hello2 echo "Hello2 from ${service_name} (${compose_name})"`
      
* docker-compose.admin.yml:

      * `# HOST_CMD hello1 echo "Hello1 from ${service_name} (${compose_name})"`
      * `# HOST_CMD hello1 echo "Hello1 again from ${service_name} (${compose_name})" && echo "Hello1 once more from ${service_name} (${compose_name})"`
      * `# HOST_CMD hello2 echo "Hello2 from ${service_name} (${compose_name})"`
      
Base on these instructions, the command

`./manage-compose-services.sh config hello1 portainer`

would produce output like the following:

```
Hello1 from portainer (docker-compose.admin.yml)
Hello1 again from portainer (docker-compose.admin.yml)
Hello1 once more from portainer (docker-compose.admin.yml)
Hello1 from portainer (docker-compose.yml)
```

Likewise, the command

`./manage-compose-services.sh config hello2 portainer`

would produce the following output:

```
Hello2 from portainer (docker-compose.admin.yml)
Hello2 from portainer (docker-compose.yml)
```

## License and disclaimer

MIT License

Copyright (c) 2018 Ruud Senden

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

