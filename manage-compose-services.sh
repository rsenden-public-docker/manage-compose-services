#! /bin/bash

# This script Performs the following actions:
# - Source the environment settings from the configuration file (if existing)
# - Set the SCRIPT_NAME, SCRIPT_DIR and SCRIPT variables (possibly overriding 
#   the settings from the configuration file)
# - Source all helper scripts in the scripts directory 
# - Invoke run_main defined in action_main.sh

[[ -e "/etc/manage-compose-services.conf" ]] && . "/etc/manage-compose-services.conf"

SCRIPT_NAME=$( basename "$0" )
SCRIPT_DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
SCRIPT="$SCRIPT_DIR/$SCRIPT_NAME"

for f in $SCRIPT_DIR/scripts/*.sh
do
   . $f
done

run_main "$@"