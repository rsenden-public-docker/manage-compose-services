#! /bin/bash

# This script provides actions for generating and removing cron jobs on the 
# host system, based on CRON instruction comments in docker-compose services 
# files.

# Available gitlab-related actions.
CRON_AVAILABLE_ACTIONS=("install" "remove")

init_main_cron() {
   main_addActions "cron"
}

# Indicate that 'cron' is a composite action, providing sub-actions
main_cron_isCompositeAction() { return 0; }

# Print generic usage information for the cron action.
main_cron_help() {
   # Every defined action has a corresponding help handler, so if
   # an action is specified, we consider it as an unknown action. 
   [[ -z "$1" ]] || handleError "Unknown action '$1'"
     
   action_printUsage "$(action_printCliArgsSubAction)"
   echo "Install or remove cron jobs based on CRON instructions in the docker-compose service files"
   echo "The following actions are supported:"
   echoElements "* " "${CRON_AVAILABLE_ACTIONS[@]}" 
   action_printSubActionHelp
}

# Show help information for cron install.
main_cron_install_help() {
   local cronFormat="<minute> <hour> <day of month> <month> <day of week> <user>"
   action_printUsage "$(_do_forServices_cliArgs)"
   echo "Install cron jobs based on CRON instructions in the docker-compose service files."
   echo "The cron jobs will be installed to /etc/cron.d/<service name>. CRON instructions"
   echo "are based on HOST_CMD instructions, allowing for scheduling automated execution"
   echo "of HOST_CMD instructions. The CRON instruction has the following format:"
   echo "# CRON ${cronFormat} <HOST_CMD name>"
   echo "" 
   echo "As an example, the following instructions would cause the current date and time"
   echo "to be recorded in /tmp/cronlog.txt at 4:30am every day:"
   echo "# HOST_CMD logDate date > /tmp/cronlog.txt"
   echo "# CRON 30 4 * * * root logDate"
}

# Show help information for cron remove.
# Parameters: none
# Context: none
main_cron_remove_help() {
   action_printUsage "$(_do_forServices_cliArgs)"
   echo "Remove cron jobs previously generated based on the CRON_* comments in" 
   echo "the docker-compose service files."
}

main_cron_install() {
   _do 'main_cron_install_forService' forServices "$@"
}

main_cron_remove() {
   _do 'main_cron_remove_forService' forServices "$@"
}

# Remove all cron job entries for the service currently being
# processed by an '_do ... forServices ...' loop.
#
# Parameters: none
# Context: _do ... forServices ...
main_cron_remove_forService() {
   echo "Removing crontab file /etc/cron.d/$service_name"
   rm -f "/etc/cron.d/$service_name"
}

# Re-create cron job entries for the service currently being
# processed by an '_do ... forServices ...' loop. This will
# first remove any old cron entries, and then generate new 
# entries and write these to a crontab file.
# Parameters: none
# Context: _do ... forServices ...
main_cron_install_forService() {
   main_cron_remove_forService
   local cronDefs=$(_do 'main_cron_install_echoScriptCmd' forCurrentServiceInstructions "docker-compose*.yml" "CRON")
   echo "Installing cron jobs:"
   echo "$cronDefs" 
   echo "$cronDefs" > /etc/cron.d/$service_name
}

main_cron_install_echoScriptCmd() {
   IFS=' ' read -r cron_min cron_hr cron_dom cron_mon cron_dow cron_user cron_cmd <<< "${instruction}"
   # TODO Check whether ${instruction} matches with an existing HOST_CMD name in any of the docker-compose*.yml files for the current service
   echo "# ${compose_name}: ${instructionName} ${instruction} ${cron}"
   echo "$cron_min $cron_hr $cron_dom $cron_mon $cron_dow $cron_user '$SCRIPT' run '$cron_cmd' '$service_name'"
}

