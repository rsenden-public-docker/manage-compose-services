#! /bin/bash

# Available gitlab-related actions.
DATA_AVAILABLE_ACTIONS=("setup")

init_main_data() {
   main_addActions "data"
}

# Indicate that 'systemd' is a composite action, providing sub-actions
main_data_isCompositeAction() { return 0; }

# Print generic usage information for the gitlab action.
main_data_help() {
   # Every defined action has a corresponding help handler, so if
   # an action is specified, we consider it as an unknown action. 
   [[ -z "$1" ]] || handleError "Unknown action '$1'"
     
   action_printUsage "$(action_printCliArgsSubAction)"
   echo "Execute one of the following actions on service data directories:"
   echoElements "* " "${DATA_AVAILABLE_ACTIONS[@]}" 
   action_printSubActionHelp
}

# Print usage information for the setup action
main_data_setup_help() {
   action_printUsage "$(_do_forServices_cliArgs)"
   echo "Set up the data directory for the given services."
   
}

main_data_setup() {
   checkConfiguredDirs
   _do 'main_data_setup_forService' forServices "$@"
}

main_data_setup_forService() {
   local data_dir_src="$compose_dir/data"
   local data_dir_dest="$SERVICES_DATA_DIR/$service_name"
   ([[ -d "$data_dir_src" && ! -L "$data_dir_src" ]] && ! rmdir "$data_dir_src") && ([[ -d "$data_dir_dest" ]] && ! rmdir "$data_dir_dest") && \
      handleError "$(echo "Both $data_dir_src and $data_dir_dest exist and contain files."; echo "Please manually merge and remove one of these directories before running data setup again.")"
      
   if [[ -d "$data_dir_src" && ! -L "$data_dir_src" ]]
   then
      echo "Moving $data_dir_src to $data_dir_dest"
      mv "$data_dir_src" "$data_dir_dest"
   elif [[ ! -d "$data_dir_dest" ]]
   then
      echo "Creating data directory $data_dir_dest" 
      mkdir -p "$data_dir_dest"
   fi
   
   [[ -e "$data_dir_src" && ! -L "$data_dir_src" ]] && handleError "$data_dir_src exists but is not a symbolic link, please manually remove before running data setup again."
   [[ -e "$data_dir_src" && "$data_dir_dest" != "$(readlink "$data_dir_src")" ]] && handleError "$data_dir_src exists but does not point to $data_dir_dest"
   if [[ ! -e "$data_dir_src" ]] 
   then
      echo "Creating symbolic link from $data_dir_src to $data_dir_dest"
      ln -s "$data_dir_dest" "$data_dir_src"
   else 
      echo "Symbolic link from $data_dir_src to $data_dir_dest already exists; nothing to do"
   fi
   
}