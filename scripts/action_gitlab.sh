#! /bin/bash

# Available gitlab-related actions.
GITLAB_AVAILABLE_ACTIONS=("cloneGroup")

GITLAB_API_PATH="api/v4"

init_main_gitlab() {
   main_addActions "gitlab"
}

# Indicate that 'gitlab' is a composite action, providing sub-actions
main_gitlab_isCompositeAction() { return 0; }

# Print generic usage information for the gitlab action.
main_gitlab_help() {
   # Every defined action has a corresponding help handler, so if
   # an action is specified, we consider it as an unknown action. 
   [[ -z "$1" ]] || handleError "Unknown action '$1'"
     
   action_printUsage "$(action_printCliArgsSubAction)"
   echo "Execute one of the following gitlab-related actions:"
   echoElements "* " "${GITLAB_AVAILABLE_ACTIONS[@]}" 
   action_printSubActionHelp
}

# Print usage information for the cloneGroup action
main_gitlab_cloneGroup_help() {
   action_printUsage "<gitlab URL> <gitlab auth token> <group name>"
   echo "Clone all repositories in a gitlab group to the docker-compose services"
   echo "directory. Parameters:"
   echo "* <gitlab URL>: Base URL for your gitlab instance, for example https://www.gitlab.com/"
   echo "* <gitlab auth token>: Gitlab personal access token"
   echo "* <group name>: Name of the group for which you want to clone all repositories"
}

main_gitlab_cloneGroup() {
   local gitlabUrl="$1";
   local gitlabAuthToken="$2";
   local gitlabGroupName="$3";
   
   checkServicesImplDir
   checkRequiredCommands "jq" "git"
   local gitlabGroupId=$(curl -s --header "PRIVATE-TOKEN: $gitlabAuthToken" $gitlabUrl/$GITLAB_API_PATH/groups?search=${gitlabGroupName} | jq '.[].id')
   [[ -z "$gitlabGroupId" ]] && handleError "Group '$gitlabGroupName' not found"
   echo "Gitlab group id: $gitlabGroupId"
   
   # TODO Handle situation when git output directory already exists (maybe add a cloneOrPullGroup action, and leave this as-is to have git show error message?)
   # TODO The sed in this pipe assumed that the repo URL starts with https://. http:// or potentially other protocols won't get the user credentials added
   (cd $SERVICES_IMPL_DIR && curl -s --header "PRIVATE-TOKEN: $gitlabAuthToken" $gitlabUrl/$GITLAB_API_PATH/groups/$gitlabGroupId/projects?per_page=100 | jq --arg p "http_url_to_repo" '.[] | .[$p]' | sed "s|https://|https://oauth2:$gitlabAuthToken@|g" | xargs -L1 git clone)
}