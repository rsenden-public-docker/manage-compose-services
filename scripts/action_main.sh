#! /bin/bash

# This script provides the following:
#  - Main entry point for running actions (run_main)
#  - Main help (main_help)
#  - Configure action for configuring the environment (main_configure*)
#  - main_addActions function to allow implementations to add main actions

# Define array that will list all main actions
declare -a MAIN_ACTIONS

# Generic action handler for 'main'. All actions should have a specific
# action handler, so this function is only called for unknown actions,
# hence we generate an error message.
# Parameters:
#  1: Action name
# Context: none
main() {
   handleError "Unknown action '$1'"
}

# Define that 'main' is a composite action
# Parameters: none
# Context: none
main_isCompositeAction() { return 0; }

# Print generic usage information
# Parameters: none
# Context: none
main_help() {
   action_printUsage "$(action_printCliArgsSubAction)"
   echo "Execute one of the following actions:"
   echoElements "* " "${MAIN_ACTIONS[@]}"
}

# Allow other scripts to add main actions. This is only used
# for listing all available main actions in the usage information.
# When the user specifies an action, this will be automatically
# mapped to a corresponding action handler.
# Parameters:
#  *: Main action names to be added
# Context: none
main_addActions() {
   MAIN_ACTIONS+=( "$@" )
}

# Perform global system configuration. This is usually called from an
# init-system specific configure action. This function creates the
# /etc/docker-compose-services.conf file with some environment settings
# for use by various actions.
#
# Parameters: See main_configure_help
# Context: none
main_configure() {
   while getopts ":s:d:i" opt
   do
      case $opt in 
         s) SERVICES_IMPL_DIR="$OPTARG" ;;
         d) SERVICES_DATA_DIR="$OPTARG" ;;
         i) SERVICES_INIT_SYSTEM="$OPTARG" ;;
         \?) handleWarning "Unknown option: -$OPTARG" ;;
      esac
   done
   [[ -z $SERVICES_IMPL_DIR ]] && SERVICES_IMPL_DIR=$SCRIPT_DIR/services/impl
   [[ -z $SERVICES_DATA_DIR ]] && SERVICES_DATA_DIR=$SCRIPT_DIR/services/data
   [[ -z $SERVICES_INIT_SYSTEM ]] && SERVICES_INIT_SYSTEM=systemd
   echo "Using the following settings:"
   echo "* Base directory for service definitions: $SERVICES_IMPL_DIR"
   echo "* Base directory for services data: $SERVICES_DATA_DIR"
   echo "* Init system: $SERVICES_INIT_SYSTEM"
   
cat > "/etc/manage-compose-services.conf" << EOF 
SCRIPT="$SCRIPT"
SCRIPT_DIR="$SCRIPT_DIR"
SCRIPT_NAME="$SCRIPT_NAME"
SERVICES_IMPL_DIR="$SERVICES_IMPL_DIR"
SERVICES_DATA_DIR="$SERVICES_DATA_DIR"
SERVICES_INIT_SYSTEM="$SERVICES_INIT_SYSTEM"
EOF

   createConfiguredDirs
   isFunction "service_${SERVICES_INIT_SYSTEM}_configure" && service_${SERVICES_INIT_SYSTEM}_configure "$@"
}

# Print the CLI arguments for the configure action
# Parameters: none
# Context: none
main_configure_cliArgs() {
   echo "[options]" 
}

# Print usage information for the configure action
# Parameters: none
# Context: none
main_configure_help() {
   action_printUsage "$(main_configure_cliArgs)" 
   echo "This action creates the /etc/docker-compose-services.conf file,"
   echo "containing some configuration settings used by various actions."
   echo "This includes the location of the ${SCRIPT_NAME} script" 
   echo "(to allow the script to be invoked from systemd unit files or similar"
   echo "for other init systems), the base directory where docker-compose" 
   echo "service directories are stored, and the base directory where data for"
   echo "services is stored. In addition, this will call the init-system specific"
   echo "configure function to allow for any init-system specific initialization."
   echo ""
   echo "Available options:"
   echo ""
   echo "-s <base directory for docker-compose services>"
   echo "   The optional -s option specifies the docker-compose services base directory."
   echo "   If this option is not present, by default the following directory will be used:"
   echo "   ${SCRIPT_DIR}/services/impl"
   echo "" 
   echo "-d <base directory for service data>"
   echo "   The optional -d option specifies the base directory where data for"
   echo "   docker-compose services is stored. If this option is not present,"
   echo "   by default the following directory will be used:"
   echo "   ${SCRIPT_DIR}/services/data"
   echo ""
   echo "-i <init system for managing services>"
   echo "   Specifies the init system used for managing docker-compose services"
   echo "   as host system services. This script supports the following init"
   echo "   systems:"
   echoElements "   * " "${SERVICES_INIT_SYSTEMS[@]}"
   echo "   If this option is not present, systemd will be used."
   echo ""  
   echo "The 'configure' action only needs to be run once, unless you change"
   echo "the location of the ${SCRIPT_NAME} script or if you want to change"
   echo "any of the options listed above."
}

# Check whether init system has been configured, calling handleError if not
checkInitSystem() {
   [[ -z "$SERVICES_INIT_SYSTEM" ]] && handleError "SERVICES_INIT_SYSTEM not defined. Please run 'configure' action first"
}

# Check whether the SERVICES_IMPL_DIR variable has been set,
# calling handleError if not.
checkServicesImplDir() {
    [[ -z "$SERVICES_IMPL_DIR" ]] && handleError "SERVICES_IMPL_DIR not defined. Please run 'configure' action first"
    [[ ! -d "$SERVICES_IMPL_DIR" ]] && handleError "Services base directory $SERVICES_IMPL_DIR does not exist"
}

# Check whether the SERVICES_IMPL_DIR variable has been set,
# calling handleError if not.
checkServicesDataDir() {
    [[ -z "$SERVICES_DATA_DIR" ]] && handleError "SERVICES_DATA_DIR not defined. Please run 'configure' action first"
    [[ ! -d "$SERVICES_DATA_DIR" ]] && handleError "Services data base directory $SERVICES_DATA_DIR does not exist"
}

checkConfiguredDirs() {
   checkServicesImplDir
   checkServicesDataDir
}

createConfiguredDirs() {
   [[ ! -d "$SERVICES_IMPL_DIR" ]] && mkdir "$SERVICES_IMPL_DIR"
   [[ ! -d "$SERVICES_DATA_DIR" ]] && mkdir "$SERVICES_DATA_DIR"
}

# Main script entry point for running actions. This function:
#  - Adds the 'configure' action
#  - Runs all functions for which the name starts with 'init_main_';
#    this allows implementations to perform initialization. Initialization
#    would include calling main_addActions for adding main actions.
#  - Invoke action_run with the current command line parameters to 
#    actually run actions, or display help information for actions.
#
# Parameters:
#  *: Action name and parameters
# Context: none
run_main() {
   main_addActions "configure"
   runFunctionsWithPrefix "init_main_"
   executeAction "$@"
}

# Execute the given main action with the given parameters. 
executeAction() {
   action_run "main" "$@"
}
