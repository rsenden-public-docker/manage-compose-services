#! /bin/bash

# This script provides functionality for running arbitrary commands for one or
# more services. The commands can either be specified on the command line, or
# as instructions in the docker-compose*.yml files.

# Available run-related actions.
RUN_AVAILABLE_ACTIONS=("cmd" "config")

# Add 'systemd' to the list of main actions upon initialization of this module.
init_main_run() {
   main_addActions "run"
}

# Print generic usage information for the systemd action.
main_run_help() {
   # Every defined action has a corresponding help handler, so if
   # an action is specified, we consider it as an unknown action. 
   [[ -z "$1" ]] || handleError "Unknown action '$1'"
     
   action_printUsage "cmd $(main_run_cmd_cliArgs)" "$(main_run_config_cliArgs)"
   echo "Run arbitrary system commands. The system command(s) to be"
   echo "run can either be specified on the command line directly"
   echo "(using the 'cmd' option), or loaded from 'HOST_CMD' instructions"
   echo "in the docker-compose files."
   action_printSubActionHelp
}

# CLI arguments for the 'run cmd' action
main_run_cmd_cliArgs() {
   echo "\"<command to run>\" <list of service names>"
}

# CLI arguments for the 'run config' action
main_run_config_cliArgs() {
   echo "<HOST_CMD name> <list of service names>"
}

# Print usage information for the 'run cmd' action
main_run_cmd_help() {
   action_printUsage "$(main_run_cmd_cliArgs)"
   echo "Run the given <command to run> for every service listed in"
   echo "<list of service names>."
}

# Print usage information for the 'run config' action
main_run_config_help() {
   action_printUsage "$(main_run_config_cliArgs)"
   echo "Run all HOST_CMD instructions with the given name, as listed"
   echo "in the docker-compose files for the given services."
}


# Indicate that 'run' is a composite action, providing sub-actions
main_run_isCompositeAction() { return 0; }

# This action runs the command given as the first parameter, for
# the services specified as remaining parameters.
main_run_cmd() {
   local runCmd="$1"; shift
   _do 'eval "${runCmd}"' forServices "$@"
}

# This action runs all commands with a given name, as defined with the
# HOST_CMD instructions in docker-compose files.
main_run() {
   local hostCmdName=$1; shift
   _do 'eval "${instruction}"' forServicesInstructions "docker-compose*.yml" "HOST_CMD $hostCmdName" "$@"
}
