#! /bin/bash

init_main_service() {
   main_addActions "service"
   runFunctionsWithPrefix "init_service_"
}

# Define array that will list all supported init systems
declare -a SERVICES_INIT_SYSTEMS

# Allow other scripts to add available init system implementations. 
# This is only used all available init systems in the usage information
# for the main configure action.
# Parameters:
#  *: Init system names to be added
# Context: none
service_addInitSystems() {
   SERVICES_INIT_SYSTEMS+=( "$@" )
}

# Define that 'service' is a composite action
# Parameters: none
# Context: none
main_service_isCompositeAction() { return 0; }

main_service() {
   checkInitSystem
   action_run "service_${SERVICES_INIT_SYSTEM}" "$@"
}

# Print generic usage information
# Parameters: none
# Context: none
main_service_help() {
   checkInitSystem
   service_${SERVICES_INIT_SYSTEM}_help
}

# This function allows for installing and uninstalling docker-compose
# services as host system services. This function is usually called
# from HOST_CMD instructions contained in the docker-compose.yml file.
#
# Parameters:
# 1) Service action to perform; can be one of the following:
#    * installAsSystemService
#      Set up the data directory, start and enable the service,
#      and install cron jobs.
#    * installAsSystemServiceNoStart
#      Same as installAsSystemService, but doesn't start the
#      service.
#    * installCronOnly
#      Set up the data directory, and install cron jobs.
#
# Default implementations for these actions are provided in this script,
# but init-system specific implementations can override these default
# implementations by providing _service_<init system>_<action> functions.
#
# Context: _do ... forServices ...
_service() {
   local serviceAction="$1"; shift;
   if isFunction "_service_${SERVICES_INIT_SYSTEM}_$serviceAction"
   then
      _service_${SERVICES_INIT_SYSTEM}_$serviceAction "$@"
   elif isFunction "_service_$serviceAction"
   then
      _service_$serviceAction "$@"
   else
      handleError "Unknown service action $serviceAction"
   fi
}

# _service action for starting and installing a docker-compose service
# as a host system service.
_service_installAsSystemService() {
   _ data setup && _ service start && _ service enable && _ cron install
}

# _service action for installing a docker-compose service as a host 
# system service. 
_service_installAsSystemServiceNoStart() {
   _ data setup && _ service enable && _ cron install
}

# _service action for installing cron jobs only. 
_service_installCronOnly() {
   _ data setup && _ cron install
}

# _service action for undoing a previous install action,
# by removing cron jobs, stopping the host system service,
# and disabling the service.
_service_uninstall() {
   _ cron remove
   _ service stop
   _ service disable
}
