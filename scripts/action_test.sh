#! /bin/bash

# This script provides various dummy actions for testing purposes. These actions
# are only visible when the ENABLE_TEST environment variable has been set to any
# non-empty value. 

compositeCmd_isCompositeAction() { return 0; }

init_main_test() {
   if [[ ! -z "$ENABLE_TEST" ]] 
   then
      main_addActions "simpleAction"
      main_addActions "compositeAction"
   fi
}

main_echo() {
   echo "$SCRIPT $@"
}

main_evail() {
   $(echo "$SCRIPT $@")
}

main_compositeAction_nested() {
   echo "Nested composite: $*"
}

main_compositeAction_nested_help() {
   echo "Help nested composite: $*"
}

main_compositeAction() {
   local action="$1"; shift;
   echo "CompositeAction action: $action"
   echo "Parameters: $*"
}

main_compositeAction_help() {
   local action="$1"; shift;
   echo "CompositeAction action help for: $action"
   echo "Parameters: $*"
}

main_simpleAction() {
   echo "SimpleAction: $*"
}

main_simpleAction_help() {
   action_printUsage "[arbitrary parameters]"
   echo "Prints the arbitrary parameters"
}

