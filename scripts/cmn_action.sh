#! /bin/bash

# This script provides various functions for invoking actions and sub-actions,
# and generating corresponding help information.

# TODO Add handleActionError function that invokes handleError and action-specific help function
# TODO Instead of using actionStack, in printUsage we can probably just print parentAction and action

# Define function SUFFIXes
ACTION_SUFFIX_HELP="help"
ACTION_SUFFIX_ISCOMPOSITEACTION="isCompositeAction"

# Define the current sub-action stack as a global array variable
actionStack=()

# Given the following parameters:
# - Parent action name
# - Action name
# - Action parameters
#
# This function will do one of the following:
# - Invoke help_parentAction_action, or if not defined or action not specified, invoke help_parentAction 
#   to print help information
# - Recursively invoke action_run if action is a composite action
#   Action is considered composite if a function isCompositeAction_parentAction_action is defined
# - Invoke the parentAction_action function, or if not defined, invoke the parentAction function
action_run() {
   local parentAction="$1"; shift;
   local action="$1"; shift
   
   if [[ -z "$action" ]] || action_isHelp "$action"; then
      # Show parent help if action is not provided or is '-h'
      action_invokeSpecificOrParentHelp "${parentAction}" ""
   else  
      # We have an action, so update the stack
      actionStack+=($action)
      
      if [[ "-h" = "$1" ]]; then
         # Show action-specific help if action is provided and first arg is '-h'
         action_invokeSpecificOrParentHelp "${parentAction}" "${action}"
      elif action_isComposite "${parentAction}_${action}"; then
         # Recursively invoke action_run for composite action
         action_run "${parentAction}_${action}" "$@"
      else
         action_invokeSpecificOrParent "${parentAction}" "${action}" "$@"
      fi
   fi
}

# Identify whether the given CLI option corresponds to '-h' or '--help'
action_isHelp() {
   [ "$1" = "-h" -o "$1" = "--help" ]
}

# Identify whether given action is a composite action, by checking whether
# a function named ${fnAction}_${ACTION_SUFFIX_ISCOMPOSITEACTION} exists
action_isComposite() {
   local fnAction="$1"
   isFunction "${fnAction}_${ACTION_SUFFIX_ISCOMPOSITEACTION}"
}

# Invoke action-specific function if defined, otherwise invoke 
# (composite) parent action function
action_invokeSpecificOrParent() {
   local fnParent="$1"; shift;
   local action="$1"; shift;
   if [[ ! -z "$action" ]] && isFunction "${fnParent}_${action}"
   then
      "${fnParent}_${action}" "$@"
   else 
      "${fnParent}" "${action}" "$@"
   fi
}

# Invoke action-specific function if defined, otherwise invoke 
# (composite) parent action function
action_invokeSpecificOrParentHelp() {
   local fnParent="$1"; shift;
   local action="$1"; shift;
   if [[ ! -z "$action" ]] && isFunction "${fnParent}_${action}_${ACTION_SUFFIX_HELP}"
   then
      "${fnParent}_${action}_${ACTION_SUFFIX_HELP}" "$@"
   else 
      "${fnParent}_${ACTION_SUFFIX_HELP}" "${action}" "$@"
   fi
}

# Print usage line(s), given one or more action parameter descriptions.
# If multiple descriptions are provided, multiple usage lines are printed.
# Parameters:
# *) One or more action parameter descriptions
action_printUsage() {
   local printedFirst="false"
   for actionArgDescription in "$@"
   do
      if [[ "false" = "$printedFirst" ]]
      then
         printf "Usage: "
         printedFirst="true"
      else
         printf "Or:    "    
      fi
      printf "%s\n" "$(action_printUsageParameters "${actionArgDescription}")"
   done
   printf "\n"
}

# Print usage parameters (current action stack and action argument descriptions)
action_printUsageParameters() {
   local actionArgs="$1"
   local actionStackString=$(join " " "${actionStack[@]}")
   if [[ -z "$actionStackString" ]]
   then
      echo "${SCRIPT_NAME} ${actionArgs}"
   else
      echo "${SCRIPT_NAME} ${actionStackString} ${actionArgs}"
   fi
}

action_printCliArgsSubAction() {
   echo "<action> [action-specific parameters]"
}

action_printSubActionHelp() {
   printf "\nUse the following command to display help information for any of the listed actions:\n"
   action_printUsageParameters "<action> -h"
}
