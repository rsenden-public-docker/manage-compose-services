#! /bin/bash

# This script defines the _do function and related functions.
# TODO Add more information

# Usage: _do '<command>' <doType> [doTypeArgs]
# This will do the following:
# - Store the given command in the _doCmd variable,
#   for later use by the _do_invokeCmd function
# - Invoke _do_<doType> with the given [doTypeArgs]
# - The _do_<doType> function can for example set
#   local variables that may be referenced during
#   execution of <command>. The _do_<doType> function
#   must call the _do_invokeCmd function to actually
#   execute the given <command>.
#
# Note that due to the delayed variable expansion
# (when <cmd> is surrounded by double quotes as
# recommended), the given <cmd> cannot directly
# access parameters passed to the surrounding function.
# (i.e. $1, $@, ...). Instead, you will need to assign
# these parameters to local variables before invoking
# the _do function; the given <cmd> can then reference
# these local variables.
#
# It is possible to nest multiple _do statements, but
# due to the necessary quoting of <command>, it is not
# recommended to do this on a single command line. 
# Instead, you can define a function that invokes the
# second _do, and use that function in the <command>
# for the first _do.
#
# Parameters:
#  1: Command to be executed; should usually be embedded
#     in single quotes to delay variable expansion.
#  2: The do type to handle the given command. A corresponding
#     function _do_<doType> must exist.
#  *: Parameters passed to the _do_<doType> function.
_do() {
   local _doCmd="$1"; shift;
   local _doType="$1"; shift;
   #[[ -z "${_doOuterCmd}" ]] && local _doOuterCmd="${_doCmd}"
   _do_${_doType} "$@"
}

# This function executes the command previously stored in 
# the _do function. This function must be called from every 
# _do_<doType> function to actually execute the command passed
# to the _do function.
#
# Parameters: none
# Context: _do
_do_invokeCmd() {
   eval "${_doCmd}"
}

# Usage: _do_withOuterCmd '_do_forOuterCmd <nestedDoType> [nestedDoTypeArgs]' <doType> [doTypeArgs]
# This function is supposed to be called from _do_<doType>
# functions that want to combine multiple _do invocations
# before executing the original _do command.
#
# This function calls _do with the _doOuterCmd as stored
# by _do_withOuterCmd as the _do command, with the given
# _do type and _do type arguments.
#
# Parameters:
#  1: The do type to handle the outer _do command
#  *: Parameters for the given do type
# Context: _do_withOuterCmd
_do_forOuterCmd() {
   _do "${_doOuterCmd}" "$@"
}

# This function stores the current (outer) _do command
# for later use by _do_forOuterCmd, and then calls the
# _do function with the given parameters. See _do_forOuterCmd
# for usage and more information.
#
# Parameters:
# *) _do parameters 
# Context: _do
_do_withOuterCmd() {
   local _doOuterCmd="${_doCmd}"
   _do "$@"
}