#! /bin/bash

# Usage: _do '<cmd>' forServices <one or more service names>
# Loop over the given docker-compose service names or directories, 
# generating the following variables before invoking the _do command:
# - service_name: The current service name
# - compose_dir: The corresponding docker-compose service directory
# - compose_file: The corresponding docker-compose.yml file with full path
# - compose_name: docker-compose.yml
#
# Parameters:
#  *: Docker-compose service names 
# Context: _do
_do_forServices() {
   _do_forServices_checkPreconditions "$@"
   for i in "$@"
   do
      local service_name=$(basename "$i")
      local compose_dir="$SERVICES_IMPL_DIR/$service_name"
      local compose_name="docker-compose.yml"
      local compose_file="$compose_dir/$compose_name"
      if [ ! -e "$compose_file" ] 
      then
         handleWarning "Compose file $compose_file not found"
      else
         _do_invokeCmd
      fi
      echo ""
   done
}

# Print the available CLI options for actions that expect a list of service names
# Parameters: none
# Context: none
_do_forServices_cliArgs() {
   echo "<list of docker-compose service names>"
}

# This function checks the following:
# - Environment configuration
# - At least one service name has been provided
# If any of these checks fail, the handleError function is called.
# Parameters:
#  *: Docker-compose service names
# Context: none
_do_forServices_checkPreconditions() {
   checkServicesImplDir
   (( $# < 1 )) && handleError "At least one docker-compose service name must be specified"
}

# Usage: _do 'doSomethingWith ${instructionName} ${instruction}' forCurrentServiceInstructions "docker-compose*.yml" "<instructionName>"
# Process instructions embedded as comments in docker-compose
# for the service currently being processed by an
# '_do ... forServices ...' loop. The command given to the _do 
# function is invoked for every matching instruction found, 
# and can reference the ${instruction} variable to access the 
# current instruction value.
#
# Parameters:
#  1: Docker-compose file pattern
#  2: Instruction name to parse from the docker-compose file
# Context: Must be called from within '_do ... forServices ...'
_do_forCurrentServiceInstructions() {
   local filePattern="$1"; shift;
   local instructionName="$1"; shift;
   for compose_file in ${compose_dir}/${filePattern}
   do
      compose_name="$( basename "$compose_file" )"
      instructions="`sed -n -e "s/^# ${instructionName} \+//p" $compose_file`"
      if [ ! -z "$instructions" ]
      then
         while read instruction; do
            _do_invokeCmd
         done <<< "$instructions"
      fi
   done
   return 0;
}

# Usage: _do 'doSomethingWith ${instructionName} ${instruction}' forServicesInstructions "docker-compose*.yml" "<instructionName>" <docker-compose service name(s)>
# Same as _do_forCurrentServiceInstructions (singular service), but this version takes
# one or more services as parameters, instead of having to be embedded in an
# '_do ... forServices ...' loop.
#
# Parameters:
#  1: Docker-compose file pattern
#  2: Instruction name to parse from the docker-compose file
#  *: Service names
# Context: none
_do_forServicesInstructions() {
   local filePattern="$1"; shift;
   local instructionName="$1"; shift;
   _do_withOuterCmd '_do_forOuterCmd forCurrentServiceInstructions "${filePattern}" "${instructionName}"' forServices "$@"
}

# Usage: _ <action> <action parameters>
# This utility function allows for executing actions, adding the service name
# currently being processed as action parameter. This allows actions to easily
# call other actions. It's primary use is for action_run.sh though, allowing
# users to easily combine regular system commands with script actions.
# Parameters:
# 1: Action to execute
# *: Action parameters, excluding the current service name
# Context: Must be called from within '_do ... forServices ...'
_() {
   executeAction "$@" "${service_name}"
}

# Usage: _dc <docker-compose action>
# This utility function allows for running docker-compose for the service
# currently being processed. 
_dc() {
   local _dcOpts="-f ${compose_dir}/docker-compose.yml"
   [[ "docker-compose.yml" != "${compose_name}" ]] && _dcOpts+=" -f ${compose_dir}/${compose_name}"
   docker-compose ${_dcOpts} "$@"
}
