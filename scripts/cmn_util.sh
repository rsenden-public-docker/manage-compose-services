#! /bin/bash

# This script provides various utility functions used by other scripts.
# See the comments for each function for more information.

# Join an array, using the first parameter as delimiter
join() { 
   local d=$1; shift; echo -n "$1"; shift; printf "%s" "${@/#/$d}"; 
}

# Check whether the first parameter matches with any of the remaining parameters
containsElement() {
   local e match="$1"; shift
   for e; do [[ "$e" == "$match" ]] && return 0; done
   return 1
}

# TODO Can we use join function above instead?
echoElements() {
   local prefix="$1"; shift;
   for i in $@
   do
      echo "${prefix}${i}"
   done  
}

# Return success if the given parameter is a function, return failure otherwise
isFunction() {
   [ "$(type -t $1)" == "function" ]
}

# Run the given function with the given (optional) arguments if the function exists
optional() {
   local fn=$1; shift;
   isFunction $fn && $fn "$@"
}

# List all available functions with the given prefix, 
# removing the given prefix from function names
# The first parameter is the function prefix to
# search for, the second parameter is an optional
# prefix to be added to the output.
listFunctionsWithPrefix() {
   compgen -A function | sed -n -e "s/^$1/$2/p"
}

# Run all functions with the prefix given as the first parameter,
# providing all remaining parameters as parameters to the functions
# being invoked.
runFunctionsWithPrefix() {
   local prefix="$1"; shift;
   for fn in $(listFunctionsWithPrefix "${prefix}" "${prefix}")
   do
      ${fn} "$@"
   done
}

checkRequiredCommands() {
   local missingCommands=()
   for i in "$@"
   do
      command -v "$i" >/dev/null 2>&1 || missingCommands+=("$i")
   done
   [[ ${#missingCommands[@]} -eq 0 ]] || handleError "$( echo 'The following required commands are unavailable:'; echoElements '* ' "${missingCommands[@]}" )"
}

# Print error message and help information, then exit
handleError() {
   echo "ERROR: $1" >&2
   exit 1
}

# Print a warning
handleWarning() {
   echo "WARN: $1" >&2
}

# Run the command echo'd by the given command and command parameters.
evalEchoCmd() {
   $(eval "$@") 
}
