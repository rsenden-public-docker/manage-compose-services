#! /bin/bash

# This script provides functionality related to the systemd init system.

# Systemd unit name
SYSTEMD_SERVICE_BASE_NAME="docker-compose"

# Add 'systemd' to the list of init systems upon initialization of this module.
init_service_systemd() {
   # TODO Check whether systemctl is available before adding systemd as supported init system
   service_addInitSystems "systemd"
}

# Configure system for running docker-compose based services as 
# systemd services:
#  - Invoke global configure action
#  - Copy systemd unit and time files to /etc/systemd/system
#  - Invoke 'systemctl daemon-reload'  
#
# Parameters:
#  *: All parameters will be passed to main_configure
# Context: none
service_systemd_configure() {
   for f in $SCRIPT_DIR/systemd/*
   do
      echo "Copying systemd file $f to /etc/systemd/system/"
      cp $f /etc/systemd/system/
   done
   systemctl daemon-reload
}

service_systemd_isCompositeAction() { return 0; }

service_systemd() {
   local cmd_systemctl="$1"; shift;
   _do "evalEchoCmd systemctl_echo_cmd $cmd_systemctl" forServices "$@"
}

# Generate a systemctl command for the current docker-compose service
systemctl_echo_cmd() {
   echo "systemctl $1 $SYSTEMD_SERVICE_BASE_NAME@$service_name.service"
}

service_systemd_help() {
   local action="$1"; shift;
   if [[ ! -z "$action" ]]
   then 
      action_printUsage "$(_do_forServices_cliArgs)" 
      echo "Execute 'systemctl ${action}' for the given service(s)."
   else
      action_printUsage "<systemctl command> $(_do_forServices_cliArgs)"
      echo "Execute the given systemctl command for the given service(s)"
      action_printSubActionHelp
   fi
}

